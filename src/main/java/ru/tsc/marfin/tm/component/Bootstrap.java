package ru.tsc.marfin.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.api.repository.ICommandRepository;
import ru.tsc.marfin.tm.api.repository.IProjectRepository;
import ru.tsc.marfin.tm.api.repository.ITaskRepository;
import ru.tsc.marfin.tm.api.repository.IUserRepository;
import ru.tsc.marfin.tm.api.service.*;
import ru.tsc.marfin.tm.command.AbstractCommand;
import ru.tsc.marfin.tm.command.project.*;
import ru.tsc.marfin.tm.command.system.*;
import ru.tsc.marfin.tm.command.task.*;
import ru.tsc.marfin.tm.command.user.*;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.enumerated.Status;
import ru.tsc.marfin.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.marfin.tm.exception.system.CommandNotSupportedException;
import ru.tsc.marfin.tm.model.Project;
import ru.tsc.marfin.tm.model.Task;
import ru.tsc.marfin.tm.repository.CommandRepository;
import ru.tsc.marfin.tm.repository.ProjectRepository;
import ru.tsc.marfin.tm.repository.TaskRepository;
import ru.tsc.marfin.tm.repository.UserRepository;
import ru.tsc.marfin.tm.service.*;
import ru.tsc.marfin.tm.util.DateUtil;
import ru.tsc.marfin.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator{

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN**");
            }
        });
    }

    {
        registry(new InfoCommand());
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new ArgumentCommand());
        registry(new CommandsCommand());
        registry(new VersionCommand());
        registry(new ExitCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserShowProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);
        initUsers();
        //initData();
        initLogger();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[___]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void initData() {
        taskService.add(new Task("TEST TASK 1", Status.COMPLETED, "Init task", DateUtil.toDate("10.11.2020")));
        taskService.add(new Task("TEST TASK 2", Status.IN_PROGRESS, "Init task", DateUtil.toDate("10.02.2021")));
        taskService.add(new Task("TEST TASK 3", Status.NOT_STARTED, "Init task", DateUtil.toDate("11.08.2021")));
        taskService.add(new Task("TEST TASK 4", Status.NOT_STARTED, "Init task", DateUtil.toDate("12.10.2021")));
        projectService.add(new Project("TEST PROJECT 1", Status.NOT_STARTED, "Init project", DateUtil.toDate("16.03.2018")));
        projectService.add(new Project("TEST PROJECT 2", Status.IN_PROGRESS, "Init project", DateUtil.toDate("23.02.2021")));
        projectService.add(new Project("TEST PROJECT 3", Status.IN_PROGRESS, "Init project", DateUtil.toDate("20.01.2021")));
        projectService.add(new Project("TEST PROJECT 4", Status.COMPLETED, "Init project", DateUtil.toDate("25.11.2020")));
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
