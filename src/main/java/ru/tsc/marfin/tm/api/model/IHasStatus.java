package ru.tsc.marfin.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
